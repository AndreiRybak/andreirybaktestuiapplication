//
//  TableController.swift
//  Test
//
//  Created by Andrei Rybak on 23.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class TableController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private struct Constants {
        static let tableCellNibName = "TableCell"
        static let tableCellReuseIdentifier = "TableCellID"
        static let dateFormat = "yyy-MM-dd"
        static let detailViewControllerIdentifier = "DetailViewControllerID"
    }

    struct Contact {
        var name: String
        var date: String
    }
    
    var contacts: [Contact] = []
    
    private let dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: Constants.tableCellNibName, bundle: nil), forCellReuseIdentifier: Constants.tableCellReuseIdentifier)
        
        let c1 = Contact(name: "Andrey", date: "2001-04-16")
        let c2 = Contact(name: "Dima", date: "1983-12-04")
        let c3 = Contact(name: "Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name", date: "1995-05-05")
        let c4 = Contact(name: "Aleksandr", date: "1944-04-04")
        let c5 = Contact(name: "Alex", date: "1999-08-25")

        contacts = [c1,c2,c3,c4,c5]
        
        let sortButton = UIBarButtonItem(title: "sort", style: UIBarButtonItemStyle(rawValue: 0)!, target: self, action: #selector(downloadSheet))
        navigationItem.setRightBarButtonItem(sortButton, animated: true)
        
    }

}


extension TableController: UITableViewDataSource {

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.tableCellReuseIdentifier, forIndexPath: indexPath) as! TableCell
        let contact = contacts[indexPath.row]
        cell.index.text = String(indexPath.row)
        cell.name.text = contact.name
        cell.date.text = contact.date
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            contacts.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tableView.reloadData()
        }
    }
}


extension TableController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let indexPath = tableView.indexPathForSelectedRow!
        let currentCell = tableView.cellForRowAtIndexPath(indexPath)! as! TableCell
        
        let nameForPass = currentCell.name.text
        let dateForPass = currentCell.date.text
        
        if let navigationController = navigationController {
            let destinationViewController = navigationController.storyboard?.instantiateViewControllerWithIdentifier(Constants.detailViewControllerIdentifier) as? DetailViewController
            
            if let destinationVC = destinationViewController {
                destinationVC.receivedName = nameForPass
                destinationVC.receivedDate = dateForPass
                navigationController.pushViewController(destinationVC, animated: true)
            }
        }
    }
    

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}

extension TableController: UIActionSheetDelegate {
   
    @objc internal func downloadSheet() {
        
        let actionSheet = UIAlertController(title: "Choose property for sort", message: nil, preferredStyle: .ActionSheet)
        
        let cancelButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in }
        actionSheet.addAction(cancelButton)
        
        let sortByNameButton: UIAlertAction = UIAlertAction(title: "Name", style: .Default)
        { action -> Void in
            self.contacts.sortInPlace({$0.name < $1.name })
            self.tableView.reloadData()
        }
        actionSheet.addAction(sortByNameButton)
        
        let sortByDateButton: UIAlertAction = UIAlertAction(title: "Date of birth", style: .Default)
        { action -> Void in
            self.dateFormatter.dateFormat = Constants.dateFormat
            self.contacts.sortInPlace({
                let date1 = self.dateFormatter.dateFromString($0.date)
                let date2 = self.dateFormatter.dateFromString($1.date)
                if date1?.compare(date2!) == NSComparisonResult.OrderedAscending {
                    return true
                } else {
                    return false
                }
            })
            self.tableView.reloadData()
        }
        actionSheet.addAction(sortByDateButton)
        
        presentViewController(actionSheet, animated: true, completion: nil)
    }

    
}
