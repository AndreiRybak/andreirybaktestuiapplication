//
//  DetailViewController.swift
//  Test
//
//  Created by Andrei Rybak on 23.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    internal var receivedName: String?
    internal var receivedDate: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if receivedDate != nil && receivedName != nil {
            nameLabel.text = receivedName
            dateLabel.text = receivedDate
        }

    }

}
