//
//  TableCell.swift
//  Test
//
//  Created by Andrei Rybak on 23.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell {

    
    @IBOutlet weak var index: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            contentView.backgroundColor = UIColor(red: 147/255, green: 224/255, blue: 250/255, alpha: 1)
        }
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            contentView.backgroundColor = UIColor(red: 217/255, green: 245/255, blue: 255/255, alpha: 1)
        }
    }
}
