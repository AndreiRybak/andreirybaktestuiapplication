//
//  CustomSegue.swift
//  Test
//
//  Created by Andrei Rybak on 22.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class CustomSegue: UIStoryboardSegue {

    override func perform() {
        let sourceVC = sourceViewController
        sourceVC.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
