//
//  ConstraintsViewController.swift
//  Test
//
//  Created by Andrei Rybak on 12.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class ConstraintsViewController: UIViewController, ButtonDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        constraintsView()?.buttonDelegate = self
        
        title = "Constraints VC"
        

        //My custom button
        modalPresentationStyle = .Popover
        let rightButton = UIBarButtonItem(title: "Collection View", style: UIBarButtonItemStyle(rawValue: 3)!, target: self, action: #selector(pushCollectionViewController))
        navigationItem.setRightBarButtonItem(rightButton, animated: true)
        
    }
    
    internal func pushSomeViewController() {
        navigationController?.pushViewController(ViewController(), animated: true)
    }
    
    private func constraintsView() -> ConstraintsView? {
        var result: ConstraintsView?
        if isViewLoaded() {
            result = view as? ConstraintsView
        }
        return result
    }
    
    func pushCollectionViewController() {
        let destinationViewController = navigationController?.storyboard?.instantiateViewControllerWithIdentifier("MyCollectionViewControllerID")// as? MyCollectionViewController
        if let destinationViewController = destinationViewController {
            navigationController!.pushViewController(destinationViewController, animated: true)
        }
    }
}

