//
//  ConstraintsView.swift
//  Test
//
//  Created by Andrei Rybak on 15.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

protocol ButtonDelegate: class {
    func pushSomeViewController()
}

class ConstraintsView: UIView {

    internal weak var buttonDelegate: ButtonDelegate?
    
    @IBOutlet weak var photo: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    
    
//    @IBAction func buttonClick(sender: AnyObject) {
//        let randomNubmer: Int = Int(arc4random_uniform(349)+1)
//        textLabel.text = randomString(randomNubmer)
//        
//        buttonDelegate?.pushSomeViewController()
//    }
//
    
    //Constraints
   
    
//    var photoWidthLandscape: NSLayoutConstraint!
//    var photoHeightLandscape: NSLayoutConstraint!
//    var photoTopLandscape: NSLayoutConstraint!
//    var photoLeftLandscape: NSLayoutConstraint!
//    var photoWidthPortrait: NSLayoutConstraint!
//    var photoHeightPortrait: NSLayoutConstraint!
//    var photoTopPortrait: NSLayoutConstraint!
//    var photoLeftPortrait: NSLayoutConstraint!
//    
//    var nameTopLandscape: NSLayoutConstraint!
//    var nameTopPortrait: NSLayoutConstraint!
//    var nameLeftLandScape: NSLayoutConstraint!
//    var nameLeftPortrait: NSLayoutConstraint!
//    var nameRightLandscape: NSLayoutConstraint!
//    var nameRightPortrait: NSLayoutConstraint!
//    
//    
//    var statusTopLandscape: NSLayoutConstraint!
//    var statusTopPortrait: NSLayoutConstraint!
//    var statusLeftLandscape: NSLayoutConstraint!
//    var statusLeftPortrait: NSLayoutConstraint!
//    var statusRightLandscape: NSLayoutConstraint!
//    var statusRightPortrait: NSLayoutConstraint!
//    
//    var textLableTopLandscape: NSLayoutConstraint!
//    var textLableTopPortrait: NSLayoutConstraint!
//    var textLableLeftLandscape: NSLayoutConstraint!
//    var textLableLeftPortrait: NSLayoutConstraint!
//    var textLableRightLandscape: NSLayoutConstraint!
//    var textLableRightPortrait: NSLayoutConstraint!
//    

    
    internal func randomString(length: Int) -> String {
        let charactersString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let charactersArray : [Character] = Array(charactersString.characters)
        
        var string = ""
        for _ in 0..<length {
            string.append(charactersArray[Int(arc4random()) % charactersArray.count])
        }
        
        return string
    }
    
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        setNeedsUpdateConstraints()
//    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        
//        translatesAutoresizingMaskIntoConstraints = false
//        photo.translatesAutoresizingMaskIntoConstraints = false
//        name.translatesAutoresizingMaskIntoConstraints = false
//        status.translatesAutoresizingMaskIntoConstraints = false
//        textLable.translatesAutoresizingMaskIntoConstraints = false
//        
//        
//        let likes = MIBadgeButton(frame: CGRect(x: 48, y: 50, width: 1, height: 1))
//        photo.addSubview(likes)
//        likes.badgeString = "8"
//    }
//    
    
    
    /*override func updateConstraints() {
        
        func setLandscapeConstraints() {
            
            NSLayoutConstraint.deactivateConstraints(constraints)
            
            let views = ["photo": photo, "view": self, "name": name, "textLable": textLable, "status": status, "descriptionLable": descriptionLable, "button": button]
            
            var allConstraints = [NSLayoutConstraint]()
            
            let photoTopLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[photo]", options: [], metrics: nil, views: views)
            let photoLeftLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[photo]", options: [], metrics: nil, views: views)
            let photoHeightLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:[photo(60)]", options: [], metrics: nil, views: views)
            let photoWidthLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("[photo(90)]", options: [], metrics: nil, views: views)
           
            allConstraints += photoTopLandscapeV
            allConstraints += photoLeftLandscapeV
            allConstraints += photoHeightLandscapeV
            allConstraints += photoWidthLandscapeV
            
            let nameTopLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:[photo]-8-[name]", options: [], metrics: nil, views: views)
            let nameLeftLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[name]", options: [], metrics: nil, views: views)
            let nameRightLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:[name]-8-[textLable]", options: [], metrics: nil, views: views)
            
            allConstraints += nameTopLandscapeV
            allConstraints += nameLeftLandscapeV
            allConstraints += nameRightLandscapeV
            
            let statusTopLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:[name]-15-[status]", options: [], metrics: nil, views: views)
            let statusLeftLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[status]", options: [], metrics: nil, views: views)
            let statusRightLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:[status]-8-[textLable]", options: [], metrics: nil, views: views)
            
            allConstraints += statusTopLandscapeV
            allConstraints += statusLeftLandscapeV
            allConstraints += statusRightLandscapeV
            
            let textLableTopLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[textLable]", options: [], metrics: nil, views: views)
            let textLableLeftLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:[photo]-8-[textLable]", options: [], metrics: nil, views: views)
            let textLableRigtLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:[textLable]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += textLableTopLandscapeV
            allConstraints += textLableLeftLandscapeV
            allConstraints += textLableRigtLandscapeV
            
            let descriptionLableWidthLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("[descriptionLable(==textLable)]", options: [], metrics: nil, views: views)
            let descriptionLableTopLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:[textLable]-8-[descriptionLable]", options: [], metrics: nil, views: views)
            //let descriptionLableLeftLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[textLable]", options: [], metrics: nil, views: views)
            let descriptionLableRightLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("H:[descriptionLable]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += descriptionLableWidthLandscapeV
            allConstraints += descriptionLableTopLandscapeV
            //allConstraints += descriptionLableLeftLandscapeV
            allConstraints += descriptionLableRightLandscapeV
            
            let buttonCenterLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("|[button]|", options: [], metrics: nil, views: views)
            let buttonBottomLandscapeV = NSLayoutConstraint.constraintsWithVisualFormat("V:[button]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += buttonCenterLandscapeV
            allConstraints += buttonBottomLandscapeV
            
            addConstraints(allConstraints)
            
            
//            //Photo both orientations
//            photoTopLandscape = NSLayoutConstraint(item: photo, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1.0, constant: 8.0)
//            photoLeftLandscape = NSLayoutConstraint(item: photo, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 8.0)
//            photoHeightLandscape = NSLayoutConstraint(item: photo, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 60.0)
//            photoWidthLandscape = NSLayoutConstraint(item: photo, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 90.0)
//           
//            //Name label landscape
//            nameTopLandscape = NSLayoutConstraint(item: name, attribute: .Top, relatedBy: .Equal, toItem: photo, attribute: .Bottom, multiplier: 1.0, constant: 8.0)
//            nameLeftLandScape = NSLayoutConstraint(item: name, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 8.0)
//            nameRightLandscape = NSLayoutConstraint(item: textLable, attribute: .Leading, relatedBy: .Equal, toItem: name, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
            
//            //Status label landscape
//            statusTopLandscape = NSLayoutConstraint(item: status, attribute: .Top, relatedBy: .Equal, toItem: name, attribute: .Bottom, multiplier: 1.0, constant: 15.0)
//            statusLeftLandscape = NSLayoutConstraint(item: status, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 8.0)
//            statusRightLandscape = NSLayoutConstraint(item: textLable, attribute: .Leading, relatedBy: .Equal, toItem: status, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            
//            //Text lable portrait
//            textLableTopLandscape = NSLayoutConstraint(item: textLable, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1.0, constant: 8.0)
//            textLableLeftLandscape = NSLayoutConstraint(item: textLable, attribute: .Leading, relatedBy: .Equal, toItem: photo, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            textLableRightLandscape = NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal, toItem: textLable, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//
//            addConstraints([photoTopLandscape, photoLeftLandscape, photoHeightLandscape, photoWidthLandscape, nameTopLandscape, nameRightLandscape, nameLeftLandScape, statusTopLandscape, statusLeftLandscape, statusRightLandscape, textLableTopLandscape, textLableRightLandscape, textLableLeftLandscape])
        }
        
        func setPortraitConstraints() {
            
            NSLayoutConstraint.deactivateConstraints(constraints)
            
            let views = ["photo": photo, "view": self, "name": name, "textLable": textLable, "status": status, "descriptionLable": descriptionLable, "button": button]
            
            var allConstraints = [NSLayoutConstraint]()
            
            //Size classes constraints
            if traitCollection.verticalSizeClass == UIUserInterfaceSizeClass.Regular {
                let photoTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-28-[photo]", options: [], metrics: nil, views: views)
                let nameTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-28-[name]", options: [], metrics: nil, views: views)
                allConstraints += photoTopPortraitV
                allConstraints += nameTopPortraitV
            } else {
                let photoTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[photo]", options: [], metrics: nil, views: views)
                let nameTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[name]", options: [], metrics: nil, views: views)
                allConstraints += photoTopPortraitV
                allConstraints += nameTopPortraitV

            }
            
            //let photoTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:|-8-[photo]", options: [], metrics: nil, views: views)
            let photoLeftPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[photo]", options: [], metrics: nil, views: views)
            let photoHeightPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:[photo(60)]", options: [], metrics: nil, views: views)
            let photoWidthPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("[photo(60)]", options: [], metrics: nil, views: views)
           
            //allConstraints += photoTopPortraitV
            allConstraints += photoLeftPortraitV
            allConstraints += photoHeightPortraitV
            allConstraints += photoWidthPortraitV
            
            let nameLeftPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:[photo]-8-[name]", options: [], metrics: nil, views: views)
            let nameRightPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:[name]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += nameLeftPortraitV
            allConstraints += nameRightPortraitV
            
            let statusTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:[name]-15-[status]", options: [], metrics: nil, views: views)
            let statusLeftPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:[photo]-8-[status]", options: [], metrics: nil, views: views)
            let statusRightPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:[status]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += statusTopPortraitV
            allConstraints += statusLeftPortraitV
            allConstraints += statusRightPortraitV
            
            let textLableTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:[photo]-8-[textLable]", options: [], metrics: nil, views: views)
            let textLableLeftPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[textLable]", options: [], metrics: nil, views: views)
            let textLableRigtPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:[textLable]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += textLableTopPortraitV
            allConstraints += textLableLeftPortraitV
            allConstraints += textLableRigtPortraitV
            
            let descriptionLableTopPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:[textLable]-8-[descriptionLable]", options: [], metrics: nil, views: views)
            let descriptionLableLeftPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:|-8-[descriptionLable]", options: [], metrics: nil, views: views)
            let descriptionLableRightPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("H:[descriptionLable]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += descriptionLableTopPortraitV
            allConstraints += descriptionLableLeftPortraitV
            allConstraints += descriptionLableRightPortraitV
            
            let buttonCenterPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("|[button]|", options: [], metrics: nil, views: views)
            let buttonBottomPortraitV = NSLayoutConstraint.constraintsWithVisualFormat("V:[button]-8-|", options: [], metrics: nil, views: views)
            
            allConstraints += buttonCenterPortraitV
            allConstraints += buttonBottomPortraitV
            
            addConstraints(allConstraints)
            
            //Photo both orientations
//            photoTopPortrait = NSLayoutConstraint(item: photo, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1.0, constant: 8.0)
//            photoLeftPortrait = NSLayoutConstraint(item: photo, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 8.0)
//            photoHeightPortrait = NSLayoutConstraint(item: photo, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 60.0)
//            photoWidthPortrait = NSLayoutConstraint(item: photo, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 60.0)
//            
//            //Name label portrait
//            nameTopPortrait = NSLayoutConstraint(item: name, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1.0, constant: 8.0)
//            nameRightPortrait = NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal, toItem: name, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            nameLeftPortrait = NSLayoutConstraint(item: name, attribute: .Leading, relatedBy: .Equal, toItem: photo, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            
//            //Status label portrait
//            statusTopPortrait = NSLayoutConstraint(item: status, attribute: .Top, relatedBy: .Equal, toItem: name, attribute: .Bottom, multiplier: 1.0, constant: 15.0)
//            statusRightPortrait = NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal, toItem: status, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            statusLeftPortrait = NSLayoutConstraint(item: status, attribute: .Leading, relatedBy: .Equal, toItem: photo, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            
//            //Text lable portrait
//            textLableTopPortrait = NSLayoutConstraint(item: textLable, attribute: .Top, relatedBy: .Equal, toItem: photo, attribute: .Bottom, multiplier: 1.0, constant: 8.0)
//            textLableLeftPortrait = NSLayoutConstraint(item: textLable, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 8.0)
//            textLableRightPortrait = NSLayoutConstraint(item: self, attribute: .Trailing, relatedBy: .Equal, toItem: textLable, attribute: .Trailing, multiplier: 1.0, constant: 8.0)
//            
//            addConstraints([photoTopPortrait, photoLeftPortrait, photoHeightPortrait, photoWidthPortrait, nameTopPortrait, nameRightPortrait, nameLeftPortrait, statusTopPortrait, statusLeftPortrait, statusRightPortrait, textLableTopPortrait, textLableRightPortrait, textLableLeftPortrait])
       
        }
        
            if UIApplication.sharedApplication().statusBarOrientation.isLandscape {
                setLandscapeConstraints()
            } else if UIApplication.sharedApplication().statusBarOrientation.isPortrait {
                setPortraitConstraints()
            }
        
        super.updateConstraints()
    }
    */
}


