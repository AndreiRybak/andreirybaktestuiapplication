//
//  CustomView.swift
//  Test
//
//  Created by Andrei Rybak on 09.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class CustomView: UIView {
    

    
    @IBOutlet weak var lightBlueSubView: UIView!

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //button.backgroundColor = UIColor.redColor()
//        
//        blueSubView.autoresizingMask = [.FlexibleBottomMargin, .FlexibleWidth, .FlexibleHeight]
//        pinkSubView.autoresizingMask = [.FlexibleTopMargin, .FlexibleHeight, .FlexibleWidth]
//        lightBlueSubView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight, .FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleLeftMargin, .FlexibleRightMargin]
//        button.autoresizingMask = [.FlexibleWidth, .FlexibleHeight, .FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleLeftMargin, .FlexibleRightMargin]
//        textField.autoresizingMask = [.FlexibleWidth,.FlexibleHeight, .FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleLeftMargin, .FlexibleRightMargin]
//        textView.autoresizingMask = [.FlexibleWidth,.FlexibleHeight, .FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleLeftMargin, .FlexibleRightMargin]
//        
//        let dynamicTextField = UITextField(frame: CGRect(x: 30, y: 229, width: 245, height: 30 ))
//        dynamicTextField.backgroundColor = UIColor.whiteColor()
//        dynamicTextField.layer.cornerRadius = 8.0
//        dynamicTextField.layer.borderWidth = 1.0
//        dynamicTextField.layer.borderColor = UIColor.greenColor().CGColor
//        dynamicTextField.placeholder = "dynamic text field"
//        dynamicTextField.autoresizingMask = [.FlexibleWidth,.FlexibleHeight, .FlexibleTopMargin, .FlexibleBottomMargin, .FlexibleLeftMargin, .FlexibleRightMargin]
//        self.addSubview(dynamicTextField)
        
        
        //gesture recognizers
        let tapOnView = UITapGestureRecognizer(target: self, action: #selector(CustomView.hideKeyboard))
        
        let tapOnLabel = UITapGestureRecognizer(target: self, action: #selector(CustomView.forwardText))
        tapOnLabel.numberOfTapsRequired = 2
        
        
        self.addGestureRecognizer(tapOnView)
        self.label.addGestureRecognizer(tapOnLabel)
        
//        dynamicTextField.delegate = self
        textView.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        if UIApplication.sharedApplication().statusBarOrientation.isLandscape {
//            button.frame = CGRect(x: 110, y: 284, width: 245, height: 28)
//        } else if UIApplication.sharedApplication().statusBarOrientation.isPortrait {
//            button.frame = CGRect(x: 40, y: 417, width: 245, height: 25)
//        }
    }
    
    @IBAction func touchDownButton(sender: AnyObject) {
        label.text = textField.text
    }
    
    @objc private func hideKeyboard() {
        endEditing(true)
        //textField.resignFirstResponder()
    }
    
    @objc private func forwardText() {
        label.text = textField.text
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        let touch = touches.first!
    
        let location = touch.locationInView(lightBlueSubView)
        
        
        if CGRectContainsPoint(label.frame, location){
            label.text = "touchesBegan"
        }
    }
    
}


//MARK: Delegates
//extension CustomView: UITextFieldDelegate {
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        label.text = textField.text
//        return true
//    }
//}

extension CustomView: UITextViewDelegate {
   
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
    
    func textViewDidChange(textView: UITextView) {
        
        var textViewAttributedText = NSMutableAttributedString(string: textView.text)
        
        let symbolForSearch = "+"
        let symbolForReplace = "-"
        
        let substringForSearch = "123"
        
        let fontAttribute = [NSForegroundColorAttributeName: UIColor.redColor()]
        let substringForReplace = NSMutableAttributedString(string: "123", attributes: fontAttribute)
        
        if textView.text.containsString(symbolForSearch) {
            if let range = textView.text.rangeOfString(symbolForSearch) {
                textView.text.replaceRange(range, with: symbolForReplace)
                textViewAttributedText = NSMutableAttributedString(string: textView.text)
            }
        }
        
        if textView.text.containsString(substringForSearch) {
            let nsText = textView.text as NSString
            let range = nsText.rangeOfString(substringForSearch)
            textViewAttributedText.replaceCharactersInRange(range, withAttributedString: substringForReplace)
        }
        
        label.attributedText = textViewAttributedText
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.characters.count + text.characters.count > 10 {
            let textViewText: NSString = textView.text + text
            let range: NSRange = NSRange(location: text.characters.count, length: textView.text.characters.count - 1)
            let substring = textViewText.substringWithRange(range)
            
            textView.text = substring as String
        }
        return true
    }
}