//
//  CustomViewController.swift
//  Test
//
//  Created by Andrei Rybak on 22.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class CustomViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.translucent = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ModalVC" {
            if let destinationVC = segue.destinationViewController as? ViewController {
                destinationVC.customLabelText = "Hello from Custom View"
            }
            
        }
    }

}
