//
//  FirstLayout.swift
//  Test
//
//  Created by Andrei Rybak on 25.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class FirstLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLayout() {
        minimumLineSpacing = 1
        sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
}
