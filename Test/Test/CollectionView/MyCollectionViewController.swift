//
//  MyCollectionViewController.swift
//  Test
//
//  Created by Andrei Rybak on 24.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit


class MyCollectionViewController: UIViewController {

    @IBOutlet private weak var collectionView: UICollectionView!
    
    private var isFirstLayoutUsed: Bool = true
    
    private let firstLayout = FirstLayout()
    private let secondLayout = SecondLayout()
    
    private struct Constants {
        static let collectionViewCellNibName = "MyCollectionViewCell"
        static let collectionViewCellReuseIdentifier = "CollectionViewCell"
    }
    
    private struct Contact {
        var name: String
        var date: String
    }
    
    private var contacts: [Contact] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.registerNib(UINib(nibName: Constants.collectionViewCellNibName, bundle: nil), forCellWithReuseIdentifier: Constants.collectionViewCellReuseIdentifier)
        setupDatasource()
        setupInitialLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupDatasource() {
        
        let c1 = Contact(name: "Andrey", date: "2001-04-16")
        let c2 = Contact(name: "Dima", date: "1983-12-04")
        let c3 = Contact(name: "Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name Long long name", date: "1995-05-05")
        let c4 = Contact(name: "Aleksandr", date: "1944-04-04")
        let c5 = Contact(name: "Alex", date: "1999-08-25")
        let b1 = Contact(name: "Andrey", date: "2001-04-16")
        let b2 = Contact(name: "Dima", date: "1983-12-04")
        let b3 = Contact(name: "Long long name Long long name Long long name Long long name Long long name", date: "1995-05-05")
        
        contacts = [c1,c2,c3,c4,c5,b1,b2,b3]
        
        collectionView.reloadData()
    }
    
    func setupInitialLayout() {
        isFirstLayoutUsed = true
        collectionView.collectionViewLayout = firstLayout
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //Fix
    //Method for detect orientation change
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if UIApplication.sharedApplication().statusBarOrientation.isPortrait {
            isFirstLayoutUsed = true
            collectionView.collectionViewLayout.invalidateLayout()
            collectionView.collectionViewLayout = firstLayout
        } else if UIApplication.sharedApplication().statusBarOrientation.isLandscape {
            isFirstLayoutUsed = false
            collectionView.collectionViewLayout.invalidateLayout()
            collectionView.collectionViewLayout = secondLayout
        }
    }
}


extension MyCollectionViewController: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.collectionViewCellReuseIdentifier, forIndexPath: indexPath) as! MyCollectionViewCell
        let contact = contacts[indexPath.row]
        cell.nameLabel.text = contact.name
        cell.dateLabel.text = contact.date
    
        return cell
    }
}

extension MyCollectionViewController: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as? MyCollectionViewCell
        
        if let myCell = cell {
            myCell.contentView.backgroundColor = UIColor(red: 255/255, green: 161/255, blue: 150/255, alpha: 1)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as? MyCollectionViewCell
        
        if let myCell = cell {
            myCell.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as? MyCollectionViewCell
        
        if let myCell = cell {
            myCell.contentView.backgroundColor = UIColor(red: 255/255, green: 211/255, blue: 207/255, alpha: 1)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as? MyCollectionViewCell
        
        if let myCell = cell {
            myCell.contentView.backgroundColor = UIColor.whiteColor()
        }
    }
}


extension MyCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        let label = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: CGFloat.max))
        label.text = contacts[indexPath.row].name
        
        var width: CGFloat?
        var height: CGFloat?
        
        //Fix
        //Replace UIScreen.mainScreen().bounds.width with collectionView.frame.width
        if UIApplication.sharedApplication().statusBarOrientation.isPortrait {
            width = collectionView.frame.width - (firstLayout.sectionInset.left + firstLayout.sectionInset.right)
            //Label height + constraints
            height = label.requiredHeight() + 8 + 8 + 16
        } else if UIApplication.sharedApplication().statusBarOrientation.isLandscape {
            width = (collectionView.frame.width / 2) - (secondLayout.sectionInset.left + secondLayout.sectionInset.right)
            height = label.requiredHeight() + 60
        }
        return CGSize(width: width!, height: height!)
    }
}



