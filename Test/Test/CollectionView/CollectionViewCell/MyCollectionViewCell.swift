//
//  MyCollectionViewCell.swift
//  Test
//
//  Created by Andrei Rybak on 25.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import QuartzCore

class MyCollectionViewCell: UICollectionViewCell {

    @IBOutlet internal weak var nameLabel: UILabel!
    @IBOutlet internal weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.borderWidth = 1
        layer.borderColor = UIColor.blackColor().CGColor
    }
    
    override func prepareForReuse() {
        contentView.backgroundColor = UIColor.whiteColor()
    }
}
