//
//  ScrollViewController.swift
//  Test
//
//  Created by Andrei Rybak on 27.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit

class ScrollViewController: UIViewController {

    @IBOutlet private weak var scrollView: UIScrollView!
    
    @IBOutlet private weak var myTextView: UITextView!
    @IBOutlet private weak var myTextViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var myLabel: UILabel!
    

    override func viewDidLoad() {
        
    }
    
    @IBAction private func forwardText(sender: AnyObject) {
        myLabel.text = myTextView.text
        myTextView.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: #selector(keyboardWillBeShown), name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillBeHidden), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillBeShown(sender: NSNotification) {
       
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as! NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets

        var aRect: CGRect = scrollView.frame
        aRect.size.height -= keyboardSize.height
        let activeTextViewRect: CGRect? = myTextView?.frame
        let activeTextViewOrigin: CGPoint? = activeTextViewRect?.origin
        if (!CGRectContainsPoint(aRect, activeTextViewOrigin!)) {
            scrollView.scrollRectToVisible(activeTextViewRect!, animated:true)
        }
    }
    
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
}

extension ScrollViewController: UITextViewDelegate {
    func textViewDidChange(textView: UITextView) {
//        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, textView.frame.height, 0.0)
//        scrollView.contentInset = contentInsets
    }
}

	