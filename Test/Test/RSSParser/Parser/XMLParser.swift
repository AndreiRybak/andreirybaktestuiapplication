//
//  RSSParser.swift
//  Test
//
//  Created by Andrei Rybak on 02.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import Foundation


class XMLParser: NSObject{

    private var insideAnItem = false
    
    private var currentElement = ""
    
    private var dataTitle = ""
    private var dataDescription = ""
    private var dataPhotoLink = ""
    
    private var currentData = Dictionary<String, String>()
    internal var parsedData = [Dictionary<String, String>]()
    
    
    func parseData(data: NSData) -> [Dictionary<String,String>] {
        let parser = NSXMLParser(data: data)
        parser.delegate = self
        parser.parse()
        return parsedData
    }
}

extension XMLParser: NSXMLParserDelegate {
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
       
        currentElement = elementName
        
        if elementName == "item" {
            insideAnItem = true
        }
        
        if elementName == "enclosure" {
            if let photoUrl = attributeDict["url"] {
                dataPhotoLink = photoUrl
            }
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        guard insideAnItem else { return }
        
        if currentElement == "title" {
            dataTitle += string
        } else if currentElement == "description" {
            dataDescription += string
        }
        
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "item" {
            insideAnItem = false
        } else if elementName == "title" && insideAnItem  {
            currentData["title"] = dataTitle
            dataTitle = ""
        } else if elementName == "description" && insideAnItem  {
            currentData["description"] = dataDescription
            dataDescription = ""
        } else if elementName == "enclosure" && insideAnItem {
            currentData["enclosure"] = dataPhotoLink
            dataPhotoLink = ""
            parsedData.append(currentData)
        }
    }
}

