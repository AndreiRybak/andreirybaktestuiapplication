//
//  News.swift
//  Test
//
//  Created by Andrei Rybak on 12.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import CoreData

@objc(MyNews)
class MyNews: NSManagedObject {
    
    @NSManaged var newsTitle: String?
    @NSManaged var newsDescription: String?
    @NSManaged var newsEnclosure: String?
    
}
