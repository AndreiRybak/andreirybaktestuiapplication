//
//  NewsList.swift
//  Test
//
//  Created by Andrei Rybak on 06.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//
//
import Foundation


class News: NSObject {

    internal private(set) var newsTitle = ""
    internal private(set) var newsDescription = ""
    internal private(set) var newsEnclosure = ""
    
    private struct PropertyKey {
        static let titleKey = "title"
        static let descriptionKey = "description"
        static let enclosureKey = "enclosure"
    }
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("news")
    
    init(title: String, description: String, enclosure: String) {
        newsTitle = title
        newsDescription = description
        newsEnclosure = enclosure
    }
    
    
    required convenience init?(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObjectForKey(PropertyKey.titleKey) as! String
        let description = aDecoder.decodeObjectForKey(PropertyKey.descriptionKey) as! String
        let enclosure = aDecoder.decodeObjectForKey(PropertyKey.enclosureKey) as! String
        
        self.init(title: title, description: description, enclosure: enclosure)
    }
    
}

extension News: NSCoding {
 
    //MARK: NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(newsTitle, forKey: PropertyKey.titleKey)
        aCoder.encodeObject(newsDescription, forKey: PropertyKey.descriptionKey)
        aCoder.encodeObject(newsEnclosure, forKey: PropertyKey.enclosureKey)
    }
}