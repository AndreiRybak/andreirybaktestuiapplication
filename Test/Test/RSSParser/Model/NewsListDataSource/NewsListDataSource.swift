//
//  NewsListDataSource.swift
//  Test
//
//  Created by Andrei Rybak on 12.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation

internal class NewsListDataSource: NewsListDataSourceProtocol {
    
    private var newsList = [News]()
    private let sessionManager = SessionManager.sharedInstance
    
    internal func provideNewsList(callbackBlock: (newsList: [News], error: NSError?) -> Void) {
        sessionManager.allNews() { [weak self] (dataArray, error) in
            guard let strongSelf = self else { return }
            
            if let dataArray = dataArray as? [News] {
                strongSelf.newsList = dataArray
                callbackBlock(newsList: strongSelf.newsList, error: error)
                NSNotificationCenter.defaultCenter().postNotificationName("reloadNewsList", object: nil)
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
}