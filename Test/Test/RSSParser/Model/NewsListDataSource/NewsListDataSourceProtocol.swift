//
//  NewsListDataSourceProtocol.swift
//  Test
//
//  Created by Andrei Rybak on 12.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation

protocol NewsListDataSourceProtocol {
    func provideNewsList(callbackBlock: (newsList: [News],error: NSError?) -> Void)
}