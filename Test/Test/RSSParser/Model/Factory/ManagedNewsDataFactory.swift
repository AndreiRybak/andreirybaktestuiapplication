//
//  ManagedDataFactory.swift
//  Test
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ManagedNewsDataFactory {
    
    internal func createNewsList(data: [Dictionary<String,String>]) {
        
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let entity = NSEntityDescription.entityForName("MyNews", inManagedObjectContext: managedContext)
        
        var title = ""
        var description = ""
        var enclosure = ""
        
        for news in data {
            
            for key in news.keys {
                switch key {
                case "title":
                    if let myTitle = news[key] {
                        title = myTitle
                    }
                case "description":
                    if let myDescription = news[key] {
                        description = myDescription.editedDescription()
                    }
                case "enclosure":
                    if let myEnclosure = news[key] {
                        enclosure = myEnclosure
                    }
                default:
                    break
                }
            }
            if let newsObject = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext) as? MyNews {
                
                newsObject.newsTitle = title
                newsObject.newsDescription = description
                newsObject.newsEnclosure = enclosure
            }
            
            do {
                try managedContext.save()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
}
    