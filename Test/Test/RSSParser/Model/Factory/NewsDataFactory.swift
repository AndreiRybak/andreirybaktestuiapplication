//
//  DataFactory.swift
//  Test
//
//  Created by Andrei Rybak on 06.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit
import CoreData

internal class NewsDataFactory {
    
    internal func createNewsList(data: [Dictionary<String,String>]) -> [News] {
        var dataArray = [News]()
        
        var title = ""
        var description = ""
        var enclosure = ""
        
        for news in data {
            
            for key in news.keys {
                switch key {
                case "title":
                    if let myTitle = news[key] {
                        title = myTitle
                    }
                case "description":
                    if let myDescription = news[key] {
                         description = myDescription.editedDescription()
                    }
                case "enclosure":
                    if let myEnclosure = news[key] {
                        enclosure = myEnclosure
                    }
                default:
                    break
                }
            }
            dataArray.append(News(title: title, description: description, enclosure: enclosure))
        }
        
        return dataArray
    }

}