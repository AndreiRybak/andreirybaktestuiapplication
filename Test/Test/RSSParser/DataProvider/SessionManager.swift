//
//  SessionManager.swift
//  Test
//
//  Created by Andrei Rybak on 05.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation

class SessionManager {
    
    //typealias CallbackBlock = (dataArray: [AnyObject], error: NSError?) -> Void
    
    private let url: NSURL
    private let session: NSURLSession
        
    private let parser = XMLParser()
    private let dataFactory = NewsDataFactory()
    private let managedDataFactory = ManagedNewsDataFactory()
    static let sharedInstance = SessionManager()

    private init() {
        session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        
        if let path = NSBundle.mainBundle().pathForResource("MyPropertyList", ofType: "plist"), myDict =  NSDictionary(contentsOfFile: path), urlString = myDict.valueForKey("URL") as? String, rssURL = NSURL(string: urlString) {
        
            url = rssURL
        } else {
            url = NSURL(string: "http://news.tut.by/rss/sport.rss")!
        }
    }
    
    internal func allNews(completion: (dataArray: [AnyObject], error: NSError?) -> Void) {
        
        session.dataTaskWithURL(url) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            
            var parsedData = [Dictionary<String,String>]()
            
            if let data = data {
                parsedData = strongSelf.parser.parseData(data)
            }
            let newsList =  strongSelf.dataFactory.createNewsList(parsedData)
            
            completion(dataArray: newsList, error: error)
        }.resume()
    }
    
    internal func allManagedNews(completion: (error: NSError?) -> Void) {
    
        session.dataTaskWithURL(url) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
    
            var parsedData = [Dictionary<String,String>]()
    
            if let data = data {
                parsedData = strongSelf.parser.parseData(data)
            }

            strongSelf.managedDataFactory.createNewsList(parsedData)
    
            completion(error: error)
            NSNotificationCenter.defaultCenter().postNotificationName("reloadNewsList", object: nil)
        }.resume()
    }
    


}