//
//  RSSTableViewController.swift
//  Test
//
//  Created by Andrei Rybak on 02.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class RSSTableViewController: UITableViewController {

    private let sessionManager = SessionManager.sharedInstance
    private var newsListDataSource: NewsListDataSourceProtocol?
    
    private var newsList = [News]()
    
    private var managedNewsList = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reloadNewsList), name:"reloadNewsList", object: nil)
        title = "News"
        loadManagedNewsList()
    }
    
    private func loadNewsList() {
        newsListDataSource = NewsListDataSource()
        newsListDataSource!.provideNewsList { [weak self] (newsList, error) in
            guard let strongSelf = self else { return }
            
            if let error = error {
                print(error.localizedDescription)
            } else {
                strongSelf.newsList = newsList
            }
        }
    }
    
    private func loadManagedNewsList() {
        sessionManager.allManagedNews(){ [weak self] (error) in
            guard let strongSelf = self else { return }
            
            let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
            let fetchRequest = NSFetchRequest(entityName: "MyNews")

            if let error = error {
                print(error.localizedDescription)
            } else {
                do {
                    let results = try managedContext.executeFetchRequest(fetchRequest)
                    strongSelf.managedNewsList = results as! [NSManagedObject]
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    internal func reloadNewsList(notification: NSNotification){
        tableView.reloadData()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return managedNewsList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("rssTableCell", forIndexPath: indexPath) as! RSSTableCell
//        cell.titleLabel.text = newsList[indexPath.row].newsTitle
//        cell.descriptionLabel.text = newsList[indexPath.row].newsDescription
        let news = managedNewsList[indexPath.row]
        cell.titleLabel.text = news.valueForKey("newsTitle") as? String
        cell.descriptionLabel.text = news.valueForKey("newsDescription") as? String
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let newsDetailView = self.storyboard?.instantiateViewControllerWithIdentifier("NewsDetailViewController") as? NewsDetailViewController {
//            newsDetailView.titleData = newsList[indexPath.row].newsTitle
//            newsDetailView.descriptionData = newsList[indexPath.row].newsDescription
            let news = managedNewsList[indexPath.row]
            newsDetailView.titleData = news.valueForKey("newsTitle") as? String
            newsDetailView.descriptionData = news.valueForKey("newsDescription") as? String

            self.navigationController?.pushViewController(newsDetailView, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: NSCoding
    func saveNews() {
        var isSuccessfulSave = false
        if let archiveURL = News.ArchiveURL.path {
            isSuccessfulSave = NSKeyedArchiver.archiveRootObject(newsList, toFile: archiveURL)
        }
        
        if !isSuccessfulSave {
            print("Failed to save news")
        }
    }
    
    func loadNews() -> [News]{
        var newsList = [News]()
        if let archiveURL = News.ArchiveURL.path {
            newsList = NSKeyedUnarchiver.unarchiveObjectWithFile(archiveURL) as! [News]
        }
        return newsList
    }

}


