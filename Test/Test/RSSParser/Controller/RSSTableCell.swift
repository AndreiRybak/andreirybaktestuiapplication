//
//  RSSTableCell.swift
//  Test
//
//  Created by Andrei Rybak on 06.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import Foundation


class RSSTableCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = UIColor.blackColor().CGColor
        layer.borderWidth = 1
    }
    
}
