//
//  NewsDetailViewController.swift
//  Test
//
//  Created by Andrei Rybak on 08.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit

class NewsDetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet private weak var detailDescription: UILabel!
    @IBOutlet private weak var detailTitle: UILabel!
    
    internal var titleData: String?
    internal var descriptionData: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let descriptionData = descriptionData{
            detailDescription.text = descriptionData
        }
        if let titleData = titleData {
            detailTitle.text = titleData
        }
    }

}
