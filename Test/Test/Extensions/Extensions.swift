//
//  Extensions.swift
//  Test
//
//  Created by Andrei Rybak on 26.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit

extension UILabel{
    
    func requiredHeight() -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, self.frame.width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }
}


extension String {
    func editedDescription() -> String {
        var editedDescription: NSString = self
        
        if editedDescription.containsString("<img src") && editedDescription.containsString("<br") {
            
            var substringStart = editedDescription.rangeOfString("<img src")
            var substirngEnd = editedDescription.rangeOfString("/>")
            var range = NSRange(location: substringStart.location, length: (substirngEnd.location + substirngEnd.length) - substringStart.location)
            
            editedDescription = editedDescription.stringByReplacingCharactersInRange(range, withString: "")
            
            substringStart = editedDescription.rangeOfString("<br")
            substirngEnd = editedDescription.rangeOfString("/>")
            range = NSRange(location: substringStart.location, length: (substirngEnd.location + substirngEnd.length) - substringStart.location)
            
            editedDescription = editedDescription.stringByReplacingCharactersInRange(range, withString: "")
        }
        
        return editedDescription as String
    }

}