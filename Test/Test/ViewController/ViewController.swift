//
//  ViewController.swift
//  Test
//
//  Created by Andrei Rybak on 17.08.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet private weak var textLabel: UILabel!
    internal var customLabelText: String?
    
    @IBOutlet weak var firstThreadLabel: UILabel!
    
    var opQueue =  NSOperationQueue()
    
    private let concurrentQueue = dispatch_queue_create("resultString queue", DISPATCH_QUEUE_CONCURRENT)
    
    private var resultString = ""

    @IBAction func dismissViewController(sender: UIButton) {
            performSegueWithIdentifier("dismissVC", sender: self)
    }
    
    @IBAction func startGCD(sender: AnyObject) {
        firstThreadLabel.text = ""
        resultString = ""
        dispatch_barrier_sync(concurrentQueue) {
            [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.resultString = strongSelf.resultString + strongSelf.numbersCycle()
        }
        dispatch_barrier_sync(concurrentQueue) {
            [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.resultString = strongSelf.resultString + strongSelf.lettersCycle()
        }
        dispatch_async(dispatch_get_main_queue()) {
            [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.firstThreadLabel.text = strongSelf.resultString
        }
    }
    
    @IBAction func startOperationQueue(sender: AnyObject) {
        firstThreadLabel.text = ""
        resultString = ""
        opQueue.addOperationWithBlock() {
            [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.resultString = strongSelf.resultString + strongSelf.numbersCycle()
            strongSelf.resultString = strongSelf.resultString + strongSelf.lettersCycle()
            NSOperationQueue.mainQueue().addOperationWithBlock(){
                strongSelf.firstThreadLabel.text = strongSelf.resultString

            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "View Controller"
        
        if let customText = customLabelText {
            textLabel.text = customText
        }
        
        let numbersCycleOperation = NumbersCycle(queuePriority: NSOperationQueuePriority.VeryLow)
        let lettersCycleOperation = LettersCycle(queuePriority: NSOperationQueuePriority.VeryHigh)
        numbersCycleOperation.addDependency(lettersCycleOperation)
        opQueue.addOperation(numbersCycleOperation)
        opQueue.addOperation(lettersCycleOperation)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc private func numbersCycle() -> String {
        return "Hello"
    }
    
    @objc private func lettersCycle() -> String {
        return "world"
    }

}

class NumbersCycle: NSOperation {
    
    init(queuePriority: NSOperationQueuePriority) {
        super.init()
        self.queuePriority = queuePriority
        
    }
    
    override func main() {
     
        if self.cancelled {
            return
        }
        
        for index in 1...5 {
            print(index)
        }
        
    }
}

class LettersCycle: NSOperation {
    
    init(queuePriority: NSOperationQueuePriority) {
        super.init()
        self.queuePriority = queuePriority
    }
    
    override func main() {
        if self.cancelled {
            return
        }
        
        for _ in 1...5 {
            print("a")
        }
    }
}